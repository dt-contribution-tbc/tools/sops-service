FROM golang:1.22.3 as builder
 
WORKDIR /go/src
COPY src .
RUN GOOS=linux CGO_ENABLED=0 go build -v -a --ldflags '-extldflags "-static"'  -o ./dist/sops .
 
FROM ghcr.io/getsops/sops:v3.8.1-alpine

WORKDIR /app

COPY --from=builder /go/src/dist/sops /usr/bin/sopsapi
COPY docker-entrypoint.sh .
RUN chmod +x docker-entrypoint.sh

ENTRYPOINT [ "/app/docker-entrypoint.sh" ]