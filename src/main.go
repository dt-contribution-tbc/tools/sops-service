package main
 
import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

type JsonResponse struct {
	File string `json:"file"`
}

func helpSops(w http.ResponseWriter, r *http.Request) {
            fmt.Fprintf(w, "This Api is used to decrypt Sops secrets")
}
 
// func decryptData(w http.ResponseWriter, r *http.Request) string {
 
//          var  body JsonResponse
//          err := json.NewDecoder(r.Body).Decode(&body)
//          if err != nil {
//                      w.WriteHeader(http.StatusBadRequest)
//                      fmt.Fprintf(w, "Error decoding request body: %v", err)
//                      return "Error"
//          }
 
//          // Decrypt the secrets file using Age
//          decryptedData, err := decrypt.Age(body.File, body.EncryptionType)
//          if err != nil {
//                      fmt.Println("Error decrypting file:", err)
//                      os.Exit(1)
//          }
//          return decryptedData
// }

func SendDecryptedFile(w http.ResponseWriter, r *http.Request) {

	var filePath JsonResponse
	err := json.NewDecoder(r.Body).Decode(&filePath)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Error decoding request body: %v", err)
		return
	}

	data, err := os.ReadFile(filePath.File)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Error reading file: %v", err)
		return
	}

	// Write the file content to the response body
	w.Write(data)
}

func main() {
	// Init the mux router
	router := mux.NewRouter()

	// Run the acmeproxy process
	router.HandleFunc("/", helpSops).Methods("GET")

	router.HandleFunc("/decrypt/", SendDecryptedFile).Methods("POST")

	fmt.Println("Server at 8070")
	log.Fatal(http.ListenAndServe("0.0.0.0:8070", router))
}